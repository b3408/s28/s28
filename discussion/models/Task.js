//Create a Schema, model for the task collection and makesure to export.
const mongoose = require('mongoose');

const taskBlueprint = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task Name is required"]
	},
	status: {
		type: String,
		default: 'pending'
	}
});

module.exports = mongoose.model("Task", taskBlueprint);