//Create a Schema, model for the user collection and makesure to export.
 const mongoose = require('mongoose');

 const userBlueprint = new mongoose.Schema({
	firstName: {
		type : String,
		required : [true, 'First name is required']
	},
	lastName: {
		type : String,
		required : [true, 'Last name is required']
	},
	email: {
		type : String,
		required : [true, 'Email is required']
	},
	password: {
		type : String,
		required : [true, 'Password is required']
	},
	isAdmin: {
		type : Boolean,
		default : false
	},
	task: [
		{
			taskId: {
				type:String,
				required: [true, 'Task Id is required']
			},
			assignedOn: {
				type: Date,
				default: new Date()
			}
		}
	] 
});


 module.exports = mongoose.model("User", userBlueprint)