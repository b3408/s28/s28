//[SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks')
	
//[SECTION] Routing Component
	const route = express.Router();

//[SECTION] TASK ROUTES
//Create TASK
	route.post('/', (req,res) => {
		//execute the createTask() from the controller
		let taskInfo = req.body
		controller.createTask(taskInfo).then(result => res.send(result));

	});

//Retrieve all Task
	route.get('/', (req,res) => {
		controller.getAllTask().then(result => {
			res.send(result);
		})
	});

//Retrieve Single Task
	route.get('/:id', (req,res) => {
			let taskId = req.params.id;
		controller.getTask(taskId).then(outcome => res.send(outcome))
			});


//Update a Task 
		route.put('/:id', (req,res)=> {
			let id = req.params.id;
			let newStatus = req.body
			controller.updateTask(id, newStatus).then(outcome => {
				res.send(outcome)
			});
		});

//Delete a Task
	route.delete('/:id', (req,res) =>{
			let taskId = req.params.id
			controller.deleteTask(taskId).then(result => res.send(result))
			});		

//[SECTION] Expose Route Sytem
	module.exports = route;