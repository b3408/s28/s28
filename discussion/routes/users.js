//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users')
	
//[SECTION] Routing Component
	const route = exp.Router();

//[SECTION] TASK ROUTES
		
	
//Create TASK
		route.post('/register', (req,res) => {
		let userData = req.body
		controller.registerUser(userData).then(result => res.send(result));
	});

//Retrieve All User
		route.get('/', (req,res) => {
			controller.getAllUsers().then(result => res.send(result))
		});

//Retrieve Single Useer
		route.get('/:id', (req,res) => {
			let userId = req.params.id;
		controller.getProfile(userId).then(outcome => res.send(outcome))
			});

//Delete a User profile
		route.delete('/:id', (req,res) =>{
		
			let userId = req.params.id
			controller.deleteUser(userId).then(result => res.send(result))
			});

//Update a User profile
		route.put('/:id', (req,res)=> {
			let id = req.params.id;
			let body = req.body
			controller.updateUser(id, body).then(outcome => {
				res.send(outcome)
			});
		});


//[SECTION] Expose Route Sytem
	module.exports = route;