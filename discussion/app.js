//[SECTION] Dependencies and Modules
	const mongoose = require('mongoose')
 	const express = require('express');
 	const dotenv = require('dotenv')
 	const tasksRoutes =	require('./routes/tasks');
 	const userRoutes =	require('./routes/users');

//[SECTION] Environment Variables
	
	dotenv.config();
	let secret = process.env.CONNECTION_STRING
	
//[SECTION] Server Setup
	const server = express();
	const port = process.env.PORT;
	 server.use(express.json()); //middleware

//[SECTION] Database Connect
	mongoose.connect(secret);

	let db = mongoose.connection;

	db.once("open", () => {
		console.log('Now Connected to MongoDB Atlas')
	});

//[SECTION] Server  Routes
  	server.use('/tasks', tasksRoutes);
  	server.use('/user', userRoutes);


//[SECTION] Server Response
	server.listen(port, () => {
  	console.log(`server is running on port: ${port}`) });

  	server.get('/', (req,res) =>{
  		res.send('Welcome to the app')
  	})

