	//[SECTION] Dependencies and Modulees
	const User = require('../models/User');
	const bcrypt =	require('bcrypt')
	



	//[SECTION] User Functionalities
	//Create a new user
	module.exports.registerUser = (reqBody) => {
		let fName = reqBody.firstName				
		let lName = reqBody.lastName	
		let email = reqBody.email
		let passW = reqBody.password
		
		let newUser = new User({
				firstName : fName,
				lastName : lName,
				email : email,
				password: bcrypt.hashSync(passW, 10)
			});
		return newUser.save().then((user,error) => {
			if (user) {
				return `A New User has beeen Created${user}`
			} else {
				return 'Failed to Create User '
			}
		})
		};

	//Retrieve All user
	module.exports.getAllUsers = () => {
		return User.find({}).then(resultOfQuery => {
			return resultOfQuery;
		});
	};
	//Retrieve a single user
	module.exports.getProfile = (data) => {

		return User.findById(data).then(result => {
			
			return result;
		})
	};

	//Delete a User
	module.exports.deleteUser = (userId) => {
		return User.findByIdAndRemove(userId).then((removedUser, error) => {
			//create a control structure that will give out thhe response according to the state of the promise.
			if (removedUser) {
				return `Account Deleted Successfully${removedUser}`;
			} else {
				return 'No accounts were removed'
			}
		})
	};

	//Update User Details
	module.exports.updateUser = (userId, newContent) => {
		//user?
		//details about the user
	let fName = newContent.firstName;			
	let lName = newContent.lastName;	

		return User.findById(userId).then((foundUser, error) =>{
			//Create a control structure that will determine the response according to the 'state' of the promise
			if (foundUser) {
				foundUser.firstName = fName;
				foundUser.lastName = lName;
				return foundUser.save().then((updatedUser, saveErr) =>{
					if (saveErr) {
						return false;
					} else {
						return updatedUser;
					}
				})
			} else {
				return 'User not found'
			}
		})
	};