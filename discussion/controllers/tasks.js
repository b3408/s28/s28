//[SECTION]Dependncies and Modules
const Task = require('../models/Task.js');


//[SECTION] Functionalities

	//Crate New Task
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name
		let newTask = new Task({
			name: taskName
		});
		return newTask.save().then((task, error)=> {
			if (error) {
				return 'There was an error creating a new task'
			} else {
				return 'Successfully created a new task'
			}
		})
	};
	//Retrieve All Tasks

	module.exports.getAllTask = () => {
		return Task.find({}).then(searchResult => {
			return searchResult;
		})
	};

	//Retrieve a single task
	module.exports.getTask = (data) => {
		return Task.findById(data).then(result => {
		return result;
		})
	};


//Update Task Status
	module.exports.updateTask = (taskId, newContent) => {
	let tstatus = newContent.status;			
		return Task.findById(taskId).then((foundTask, error) =>{
			if (foundTask) {
				foundTask.status = tstatus;
			
				return foundTask.save().then((updatedTask, saveErr) =>{
					if (saveErr) {
						return false;
					} else {
						return updatedTask;
					}
				})
			} else {
				return 'Task not found'
			}
		})
	};



//Delete a Task
	module.exports.deleteTask = (taskId) => {
		return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
			if (removedTask) {
				return `Task Deleted Successfully${removedTask}`;
			} else {
				return 'No task were removed'
			}
		})
	};




